import { Component, OnInit, Input } from '@angular/core';
import { ExpenseService } from '../service/expense.services';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmationDialogService } from '../confirmation-dialog/confirmation-dialog.component.service';

@Component({
  selector: 'app-expense-summary',
  templateUrl: './expense-summary.component.html',
  styleUrls: ['./expense-summary.component.css']
})

export class ExpenseSummaryComponent implements OnInit {
    @Input() userId;
    expenseList: any;
    id: number;
  _Activatedroute: any;

  constructor(private expenseService: ExpenseService, private router: Router, private activatedRoute: ActivatedRoute, _Activatedroute:ActivatedRoute, private confirmationDialogService: ConfirmationDialogService) { }

  ngOnInit() {

    this.activatedRoute.params.subscribe(paramsId => {
      this.userId = +paramsId.userId;
      });
      
    this.getExpenseSummary();
  }

  getExpenseSummary(){
    this.expenseService.getExpenseByUserId(this.userId).subscribe(
      response => {
          console.log(response);
          if(response){
              this.expenseList = response
          }
      });
  }

  expenseDetails(expenseId){
    this.id = expenseId;
    this.router.navigate(['expenseDetails', this.id]);
  }

  public openConfirmationDialog(id) {
    this.confirmationDialogService.confirm('Please confirm..', 'Do you really want to delete this expense? This cannot be undone.')
    .then((confirmed) => this.deleteExpense(id))
    .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
  }

  deleteExpense(id) {
    this.expenseService.deleteExpense(id).subscribe(
      response => {
        console.log(response);
        this.getExpenseSummary();
        }
    )
  }

  addExpense(userId){
    this.userId = userId;
    this.router.navigate(['expense', this.userId]);
  }

  goToDashboard(){
    this.router.navigate(['dashboard']);
  }

}
