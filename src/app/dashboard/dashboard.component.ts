import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
    userDetails : any;
    selectedIndex: number = 0;
    demo1TabIndex : any;

  constructor() { }

  ngOnInit() {
    this.userDetails = (JSON.parse(localStorage.getItem('currentUser')));

  }

  // nextStep() {
  //   if (this.selectedIndex != 4) { //4 is the maximum number of tabs
  //     this.selectedIndex = this.selectedIndex + 1;
  //   }
  //   console.log(this.selectedIndex);
  // }

  // previousStep() {
  //   if (this.selectedIndex != 0) {
  //     this.selectedIndex = this.selectedIndex - 1;
  //   }
  //   console.log(this.selectedIndex);
  // }


  nextStep(){
    this.demo1TabIndex = 2;
  }
}
