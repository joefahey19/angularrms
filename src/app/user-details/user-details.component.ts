import { UserDetailsConfirmationServices } from './../user-details-confirmation/user-details-confirmation.component.services';
import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../service/user.services';
import { UpdatedUser } from '../models/edit-user';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  userObject: any;
  userDetails: any;
  user : UpdatedUser;
  fName: any;
  lName: any;
  email: any;
  dateOfBirth: any;
  jobTitle: any;
  ppsn: any;
  disabled: boolean = true;
  showCancel: boolean = false;
  userDetailsFormGroup = new FormGroup({
    formFName: new FormControl('', Validators.required),
    formLName: new FormControl('', Validators.required),
    formEmail: new FormControl('', [
      Validators.required,
      Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")
    ]),
    formJobTitle: new FormControl('', Validators.required),
    formDOB: new FormControl('', Validators.required),
    formPPSN: new FormControl('', Validators.required),
  })  
  
  constructor(private userService:
     UserService, private router: Router, private detailsConfirmService: UserDetailsConfirmationServices) { }

  ngOnInit() {

    this.userDetails = (JSON.parse(localStorage.getItem('currentUser')));

    this.userService.updateUser(this.userDetails.id).subscribe(
      response => {
        console.log(response); 
        if(response){
          this.userObject = response;
          this.userDetailsFormGroup.controls['formFName'].setValue(this.userObject.firstName);
          this.userDetailsFormGroup.controls['formFName'].disable();
          this.userDetailsFormGroup.controls['formLName'].setValue(this.userObject.lastName);
          this.userDetailsFormGroup.controls['formLName'].disable();
          this.userDetailsFormGroup.controls['formEmail'].setValue(this.userObject.emailAddress);
          this.userDetailsFormGroup.controls['formEmail'].disable();
          this.userDetailsFormGroup.controls['formJobTitle'].setValue(this.userObject.jobTitle);
          this.userDetailsFormGroup.controls['formJobTitle'].disable();
          this.userDetailsFormGroup.controls['formDOB'].setValue(this.userObject.dateOfBirth);
          this.userDetailsFormGroup.controls['formDOB'].disable();
          this.userDetailsFormGroup.controls['formPPSN'].setValue(this.userObject.taxNo);
          this.userDetailsFormGroup.controls['formPPSN'].disable();
        }
        }
    )}

    get firstName() {
      return this.userDetailsFormGroup.get('formFName')
    }
  
    get lastName() {
      return this.userDetailsFormGroup.get('formLName')
    }
    
    get emailAddress() {
      return this.userDetailsFormGroup.get('formEmail')
    }

    get formJobTitle() {
      return this.userDetailsFormGroup.get('formJobTitle')
    }

    submitForm() {
      this.user = {
        firstName: this.userDetailsFormGroup.get('formFName').value || this.userDetails.firstName,
        lastName: this.userDetailsFormGroup.get('formLName').value || this.userDetails.lastName,
        jobTitle: this.userDetailsFormGroup.get('formJobTitle').value || this.userDetails.jobTitle,
        emailAddress: this.userDetailsFormGroup.get('formEmail').value || this.userDetails.emailAddress,
        dateOfBirth: this.userDetails.dateOfBirth,
        taxNo: this.userDetails.taxNo,
        password: this.userDetails.password,
        isActive: this.userDetails.active,
        isAdmin: this.userDetails.admin,
        income: this.userDetails.income,
        outgoing: this.userDetails.outgoing,
        id: this.userDetails.id
    }

    console.log(this.user); 

    this.userService.updateDetails(this.user);
    alert('User Details Updated Succesfully')
    this.router.navigate(['/dashboard']);

  }

    fNameValueChanged(event){
      this.fName = event.srcElement.value;
    }
  
    lNameValueChanged(event){
      this.lName = event.srcElement.value;
    }
  
    emailValueChanged(event) {
      this.email = event.srcElement.value;
    }
  
    dateOfBirthValueChanged(event){
      this.dateOfBirth = event.srcElement.value;
    }
  
    jobTitleValueChanged(event){
      this.jobTitle = event.srcElement.value;
      console.log(event.srcElement.value);
    }
  
    ppsValueChanged(event){
      this.ppsn = event.srcElement.value;
    }

    inputEnabled(){
      this.userDetailsFormGroup.controls['formFName'].enable();
      this.userDetailsFormGroup.controls['formLName'].enable();
      this.userDetailsFormGroup.controls['formEmail'].enable();
      this.userDetailsFormGroup.controls['formJobTitle'].enable();
      this.showCancel = true;
      return this.disabled = false;
     
    }

    inputDisabled(){
      this.userDetailsFormGroup.controls['formFName'].disable();
      this.userDetailsFormGroup.controls['formLName'].disable();
      this.userDetailsFormGroup.controls['formEmail'].disable();
      this.userDetailsFormGroup.controls['formJobTitle'].disable();
      this.showCancel = false;
      return this.disabled = true;
     
    }

    goToDashboard(){
      this.router.navigate(['dashboard']);
    }

    openConfirmationDialog() {
      this.detailsConfirmService.confirm('Please confirm..', 'Please enter your password to save changes.')
      .then((confirmed) => this.submitForm())
      .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
    }

}
