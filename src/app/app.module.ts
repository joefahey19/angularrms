import { UserDetailsConfirmationServices } from './user-details-confirmation/user-details-confirmation.component.services';
import { SummaryComponent } from './summary/summary.component';
import { AuthService } from './service/auth.service';
import { IncomeSummaryComponent } from './income-summary/income-summary.component';
import { ExpenseComponent } from './expense/expense.component';
import { IncomeComponent } from './income/income.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomePageComponent } from './homePage/homePage.component';
import { RegisterComponent } from './register/register.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ChartsModule } from 'ng2-charts/';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSidenavModule, MatNavList, MatListModule, MatTabsModule } from '@angular/material';
import { ExpenseSummaryComponent } from './expense-summary/expense-summary.component';
import { AuthGuardService } from './service/auth-guard.service';
import { UserDetailsComponent } from './user-details/user-details.component';
import { ExpenseDetailsComponent } from './expense-details/expense-details.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { IncomeDetailsComponent } from './income-details/income-details.component';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { ConfirmationDialogService } from './confirmation-dialog/confirmation-dialog.component.service';
import { AboutComponent } from './about/about.component';
import { BarChartComponent } from './bar-chart/bar-chart.component';
import { UserDetailsConfirmationComponent } from './user-details-confirmation/user-details-confirmation.component';
import { YesNoPipe } from './yes-no.pipe';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    HomePageComponent,
    LoginComponent, 
    DashboardComponent, 
    RegisterComponent,
    IncomeComponent,
    ExpenseComponent, 
    IncomeSummaryComponent, 
    ExpenseSummaryComponent, 
    SummaryComponent, 
    UserDetailsComponent, 
    ExpenseDetailsComponent,
    IncomeDetailsComponent,
    ConfirmationDialogComponent,
    AboutComponent,
    BarChartComponent,
    UserDetailsConfirmationComponent,
    YesNoPipe,
    
  ],
  imports: [
    BrowserModule,
    ChartsModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatListModule,
    MatTabsModule,
    FontAwesomeModule, 

  ],
  entryComponents: [ConfirmationDialogComponent, UserDetailsConfirmationComponent],
  providers: [AuthGuardService, AuthService, ConfirmationDialogService, UserDetailsConfirmationServices],
  bootstrap: [AppComponent]
})
export class AppModule { }
