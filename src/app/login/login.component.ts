import { Component, OnInit } from '@angular/core';
import { UserService } from '../service/user.services';
import { Router } from '@angular/router';
import { Auth } from '../models/auth';
import { User } from '../models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    email: string;
    ppsn: string;
    password: string;
    users: any;
    showSuccessMessage : boolean;
    showFailureMessage : boolean;
    authObject: Auth;

  constructor(private userService: UserService, private router:Router) { }

  ngOnInit() {

  }

  submitForm(){
      console.log("Login button clicked");
      this.userService.getUsers().subscribe(
        response =>this.handleSuccessfulResponse(response),
       );
  }

  handleSuccessfulResponse(response) {
    this.users=response;
    
    for(let entry of this.users){

        if(entry.emailAddress == this.email && entry.taxNo == this.ppsn){
          console.log(entry);

          this.authObject =  new Auth();
          this.authObject.user = entry;
          this.authObject.password = this.password;

          this.userService.authenticateUser(this.authObject).subscribe(
            response => {
              if(response === true){
                localStorage.setItem('currentUser', JSON.stringify(entry));
                this.router.navigate(['/dashboard']);
              }else {
                this.router.navigate(['/#']);
              }
            }
           );

        }
        
    }


}

  emailValueChanged(event) {
    //console.log(event.srcElement.value);
    this.email = event.srcElement.value;
  }

  ppsValueChanged(event) {
    //console.log(event.srcElement.value);
    this.ppsn = event.srcElement.value;
  }

  passwordValueChanged(event) {
    //console.log(event.srcElement.value);
    this.password = event.srcElement.value;
  }

  }
