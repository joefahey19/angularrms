import { UserService } from './../service/user.services';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Auth } from '../models/auth';

@Component({
  selector: 'app-user-details-confirmation',
  templateUrl: './user-details-confirmation.component.html',
  styleUrls: ['./user-details-confirmation.component.css']
})
export class UserDetailsConfirmationComponent implements OnInit {
  authObject: any;
  userDetails: any;
  password: string;

  constructor(private activeModal: NgbActiveModal, private userService: UserService) { }

  ngOnInit() {
    this.userDetails = (JSON.parse(localStorage.getItem('currentUser')));
  }

  public decline() {
    this.activeModal.close(false);
  }

  public accept() {
    this.activeModal.close(true);
  }

  authenticate(){
    this.authObject =  new Auth();
          this.authObject.user = this.userDetails;
          this.authObject.password = this.password;


    this.userService.authenticateUser(this.authObject).subscribe(
      response => {
        if(response === true){
          this.activeModal.close(true)
        }else {
          alert('Password Incorrect');
        }
      }
     );
  }

  public dismiss() {
    this.activeModal.dismiss();
  }

  passwordValueChanged(event) {
    //console.log(event.srcElement.value);
    this.password = event.srcElement.value;
  }

}
