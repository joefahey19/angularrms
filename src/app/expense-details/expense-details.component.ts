import { ExpenseDetails } from '../models/edit-expense';
import { Component, OnInit, Input } from '@angular/core';
import { ExpenseService } from '../service/expense.services';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-expense-details',
  templateUrl: './expense-details.component.html',
  styleUrls: ['./expense-details.component.css']
})
export class ExpenseDetailsComponent implements OnInit {
  id : number
  expenseObject: any
  disabled: boolean = true;
  expense: ExpenseDetails;
  userDetails: any;
  user: any;

  constructor(private expenseService: ExpenseService, private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {

    this.userDetails = (JSON.parse(localStorage.getItem('currentUser')));

    this.activatedRoute.params.subscribe(paramsId => {
    this.id = paramsId.id;
    });
    console.log(this.id);
    let expenseId = +this.id;

    this.expenseService.getExpenseById(expenseId).subscribe(
      response => {
        console.log(response); 
        if(response){
          this.expenseObject = response
          }
        }
    )
  }

  submitForm(f) {
    this.expense = {
        expenseType : f.value.expenseType || this.expenseObject.expenseType,
        dateOfExpense : f.value.dateOfExpense || this.expenseObject.dateOfExpense,
        expenseValue : f.value.expenseValue ||  this.expenseObject.expenseValue,
        notes : f.value.notes || this.expenseObject.notes,
        userId : this.userDetails.userId,
        id: this.expenseObject.id
      }

      console.log(this.expense); 

      this.expenseService.updateExpenseDetails(this.expense);
      this.router.navigate(['/expenseDetails']);
  }

  inputEnabled(){
    return this.disabled = false;
    
   }

}
