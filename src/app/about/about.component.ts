import { Component, OnInit } from '@angular/core';
import { EmailService } from '../service/email.services';
import { ContactForm } from '../models/contact-form';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  constructor(private emailService: EmailService) { }
  name: string;
  email: string;
  phone: number;
  message: string;
  allInfo: string;
  contactForm: ContactForm;
  formNameValue: string; 
  contactFormGroup = new FormGroup({

    formName: new FormControl('', Validators.required),
    formEmail: new FormControl('' ,[
        Validators.required,
        Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")
      ]),
    formPhone: new FormControl('', [
      Validators.required,
      Validators.pattern("^[0-9]*$")
    ]),
    formMessage: new FormControl('', Validators.required)
  })

  ngOnInit() {

  }

  get emailAddress(){
    return this.contactFormGroup.get('formEmail')
  }

  get fullName(){
    return this.contactFormGroup.get('formName')
  }

  get phoneNumber(){
    return this.contactFormGroup.get('formPhone')
  }

  get phoneMessage(){
    return this.contactFormGroup.get('formMessage')
  }

  
  processForm() {
    alert("Thank you for your message!");

    this.contactForm = {
      name: this.contactFormGroup.get('formName').value,
      email: this.contactFormGroup.get('formEmail').value,
      phone: this.contactFormGroup.get('formPhone').value,
      message: this.contactFormGroup.get('formMessage').value

    }

    this.emailService.postContactForm(this.contactForm);

  }

  isSubmitDisabled() {
    if (this.contactFormGroup.get('formName').value && this.contactFormGroup.get('formEmail').value && this.contactFormGroup.get('formPhone').value && this.contactFormGroup.get('formMessage').value) {
      return false
    } else {
      return true;
    }
  }
}
