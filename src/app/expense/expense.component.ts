import { Component, OnInit, Input } from '@angular/core';
import { Expense } from '../models/expense';
import { ExpenseService } from '../service/expense.services';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-expense',
  templateUrl: './expense.component.html',
  styleUrls: ['./expense.component.css']
})
export class ExpenseComponent implements OnInit {
    @Input() userId;
    expense: Expense;
    hideForm: boolean = false;
    submittedExpense : any;
    
  constructor(private expenseService: ExpenseService, private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {

    this.activatedRoute.params.subscribe(paramsId => {
      this.userId = +paramsId.userId;
      });
      console.log(this.userId);
  }

  submitForm(f) {
    this.expense = {
        expenseType : f.value.expenseType,
        dateOfExpense : f.value.dateOfExpense,
        expenseValue : f.value.expenseValue,
        notes : f.value.notes,
        userId : this.userId,

  }

    this.expenseService.postExpense(this.expense).subscribe(
      response =>this.getSubmittedExpense(response),
    );

  }

  getSubmittedExpense(response) {
      
    this.expenseService.getExpenseById(response).subscribe(
      response => {
        this.submittedExpense = response;
        this.hideForm = true;
      });
  }

  refreshForm() {
    this.hideForm = false;
  }

  goToDashboard(){
    this.router.navigate(['dashboard']);
  }

  goToExpenseSummary(){
    this.router.navigate(['expense-summary', this.userId])
  }



  }
