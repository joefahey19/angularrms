import { BarChartComponent } from './bar-chart/bar-chart.component';
import { AboutComponent } from './about/about.component';
import { ExpenseComponent } from './expense/expense.component';
import { IncomeComponent } from './income/income.component';
import { ExpenseDetailsComponent } from './expense-details/expense-details.component';
import { RegisterComponent } from './register/register.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user/user.component';
import { HomePageComponent } from './homePage/homePage.component';
import { AuthGuardService as AuthGuard } from './service/auth-guard.service';
import { UserDetailsComponent } from './user-details/user-details.component';
import { IncomeDetailsComponent } from './income-details/income-details.component';
import { IncomeSummaryComponent } from './income-summary/income-summary.component';
import { ExpenseSummaryComponent } from './expense-summary/expense-summary.component';


const routes: Routes = [

  {path: 'user', component: UserComponent },
  {path: '', component: HomePageComponent,},
  {path: 'login', component: LoginComponent}, 
  {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
  {path: 'register', component: RegisterComponent},
  {path: 'userDetails', component: UserDetailsComponent},
  {path: 'expenseDetails/:id', component: ExpenseDetailsComponent},
  {path: 'invoiceDetails/:id', component: IncomeDetailsComponent},
  {path: 'income/:userId', component: IncomeComponent},
  {path: 'expense/:userId', component: ExpenseComponent},
  {path: 'income-summary/:userId', component: IncomeSummaryComponent},
  {path: 'expense-summary/:userId', component: ExpenseSummaryComponent},
  {path: 'about', component: AboutComponent},
  {path: 'bar-chart', component: BarChartComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
