import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule, By } from '@angular/platform-browser';
import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { RegisterComponent } from './register.component';
import { DebugElement } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from '@angular/router';



describe('Register Form', () => {
    let component: RegisterComponent;
    let fixture: ComponentFixture<RegisterComponent>;
    let de: DebugElement;
    let el: HTMLElement;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                RegisterComponent
            ],
            imports: [
                BrowserModule,
                FormsModule,
                ReactiveFormsModule,
                HttpClientTestingModule,
            ],
            providers: [
                { 
                    provide: Router, useClass: class { navigate = jasmine.createSpy("navigate"); }
                }
            ]
        }).compileComponents().then(() => {
            fixture = TestBed.createComponent(RegisterComponent);
            component = fixture.componentInstance;
            de = fixture.debugElement.query(By.css('form'));
            el = de.nativeElement;
        });
    }));

    it('should create Register component and access the text variable', async(() => {
        expect(component.text).toEqual('register form');
    }));

    it ('form should be valid', async(() => {
        component.registerFormGroup.controls['formFName'].setValue('Caroline');
        component.registerFormGroup.controls['formLName'].setValue('Nolan');
        component.registerFormGroup.controls['formEmail'].setValue('c@gmail.com');
        component.registerFormGroup.controls['formDOB'].setValue('29/09/1990');
        component.registerFormGroup.controls['formPPSN'].setValue('1234567O');
        component.registerFormGroup.controls['formJobTitle'].setValue('Dev');
        component.registerFormGroup.controls['formPassword'].setValue('Password1!');
        component.registerFormGroup.controls['formConfirmPassword'].setValue('Password1!');
        expect(component.registerFormGroup.valid).toBeTruthy();
    }));

    it ('form should be invalid', async(() => {
        component.registerFormGroup.controls['formFName'].setValue('Caroline');
        component.registerFormGroup.controls['formLName'].setValue('Nolan');
        component.registerFormGroup.controls['formEmail'].setValue('c"gmail.com');
        component.registerFormGroup.controls['formDOB'].setValue('01/10/1990');
        component.registerFormGroup.controls['formPPSN'].setValue('1234567O');
        component.registerFormGroup.controls['formJobTitle'].setValue('Dev');
        component.registerFormGroup.controls['formPassword'].setValue('Password1!');
        component.registerFormGroup.controls['formConfirmPassword'].setValue('Password1!');
        expect(component.registerFormGroup.valid).toBeFalsy();
    }));

    it ('password should be invalid', async(() => {

        component.registerFormGroup.controls['formPassword'].setValue('Missing4Special!');
        expect(component.registerFormGroup.valid).toBeFalsy();
    }));

    it ('password match should be invalid', async(() => {

        component.registerFormGroup.controls['formPassword'].setValue('Password1!');
        component.registerFormGroup.controls['formConfirmPassword'].setValue('Password1!');
        expect(component.registerFormGroup.valid).toBeFalsy();
    }));

});