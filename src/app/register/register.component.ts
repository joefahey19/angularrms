import { EmailService } from './../service/email.services';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../service/user.services';
import { User } from '../models/user';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { RegisterEmail } from '../models/register-email';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  text = 'register form';
  fName: string;
  lName: string;
  email: string;
  ppsn: string;
  password: string;
  dateOfBirth: string;
  jobTitle: string;
  users: any;
  user: User;
  form: any;
  newUser: any;
  emailObject: any;
  registerFormGroup = new FormGroup({

    formFName: new FormControl('', Validators.required),
    formLName: new FormControl('', Validators.required),
    formEmail: new FormControl('', [
      Validators.required,
      Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")
    ]),
    formDOB: new FormControl('', [
      Validators.required,
      Validators.pattern("^(0?[1-9]|[12][0-9]|3[01])(/|-)(0?[1-9]|1[012])(/|-)((19|20)\\d\\d)")
    ]),
    formPPSN: new FormControl('', [
      Validators.required,
      Validators.pattern("^([0-9]{7})([A-Z]|([A-Z]{2}))")
    ]),
    formJobTitle: new FormControl('', Validators.required),
    formPassword: new FormControl('', [
      Validators.required,
      Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)
    ]),
    formConfirmPassword: new FormControl('', Validators.required),
  })

  constructor(private userService: UserService, private router: Router, private emailService: EmailService) { }

  ngOnInit() {

  }

  get firstName() {
    return this.registerFormGroup.get('formFName')
  }

  get lastName() {
    return this.registerFormGroup.get('formLName')
  }

  get emailAddress() {
    return this.registerFormGroup.get('formEmail')
  }

  get formDOB() {
    return this.registerFormGroup.get('formDOB')
  }

  get formPPSN() {
    return this.registerFormGroup.get('formPPSN')
  }

  get formJobTitle() {
    return this.registerFormGroup.get('formJobTitle')
  }

  get formPassword() {
    return this.registerFormGroup.get('formPassword')
  }

  get formConfirmPassword() {
    return this.registerFormGroup.get('formConfirmPassword')
  }

  checkPasswordsMatch() {
    if (this.registerFormGroup.get('formConfirmPassword').valid && this.registerFormGroup.get('formPassword').valid) {
      if (this.registerFormGroup.get('formConfirmPassword').value === this.registerFormGroup.get('formPassword').value) {
        return true;
      } else {
        return false;
      }
    }
    return false;
  }

  isSubmitDisabled() {
    if (this.registerFormGroup.get('formFName').value && this.registerFormGroup.get('formLName').value
      && this.registerFormGroup.get('formJobTitle').value && this.registerFormGroup.get('formPassword').value
      && this.registerFormGroup.get('formEmail').value && this.registerFormGroup.get('formDOB').value
      && this.registerFormGroup.get('formPPSN').value) {
      return false
    } else {
      return true;
    }
  }

  submitForm() {
    this.user = {
      firstName: this.registerFormGroup.get('formFName').value,
      lastName: this.registerFormGroup.get('formLName').value,
      jobTitle: this.registerFormGroup.get('formJobTitle').value,
      password: this.registerFormGroup.get('formPassword').value,
      emailAddress: this.registerFormGroup.get('formEmail').value,
      isActive: true,
      isAdmin: false,
      income: 0,
      outgoing: 0,
      dateOfBirth: this.registerFormGroup.get('formDOB').value,
      taxNo: this.registerFormGroup.get('formPPSN').value

    }
    console.log(this.user);

    this.sendRegisterEmail(this.user);

    this.userService.postUser(this.user).subscribe( 
      response => {
          this.newUser = response;
          console.log(this.newUser);
          localStorage.setItem('currentUser', JSON.stringify(this.newUser));
          this.router.navigate(['/dashboard']);
      })

  }

  sendRegisterEmail(user){

    this.emailObject = new RegisterEmail;
    this.emailObject.fName = this.user.firstName;
    this.emailObject.emailAddress = this.user.emailAddress;

    this.emailService.postRegisterEmail(this.emailObject);

  }

}
