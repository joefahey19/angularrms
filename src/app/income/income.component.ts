import { Income } from './../models/income';
import { Component, OnInit, Input } from '@angular/core';
import { IncomeService } from '../service/income.services';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-income',
  templateUrl: './income.component.html',
  styleUrls: ['./income.component.css']
})
export class IncomeComponent implements OnInit {
    @Input() userId;
    showDateOfPayment : boolean = false;
    income : Income;
    formSuccess : boolean = false;
    hideForm: boolean = false;
    submittedIncome: any;
    paymentReceivedValue: any;

  constructor(private incomeService: IncomeService, private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {    
    
    this.activatedRoute.params.subscribe(paramsId => {
    this.userId = +paramsId.userId;
    });
  }

  showPaymentDate(){
      this.showDateOfPayment = true;
  }

  hidePaymentDate() {
      this.showDateOfPayment = false;
  }

  submitForm(f) {
    this.income = {
        clientName : f.value.clientName,
        dateOfInvoice : f.value.dateOfInvoice,
        invoiceValue : f.value.invoiceValue,
        paymentReceived : f.value.paymentReceived,
        dateOfPayment : f.value.dateOfPayment,
        notes : f.value.notes,
        userId : this.userId

      }

    this.incomeService.postIncome(this.income).subscribe(
      response =>this.getSubmittedIncome(response),
    );


    }

    getSubmittedIncome(response) {
      
      
      this.incomeService.getByIncomeId(response).subscribe(
        response => {
          this.paymentReceived(response);
          this.submittedIncome = response;
          this.hideForm = true;
        });
    }

    paymentReceived(response){
      if (response) {
      this.submittedIncome = response;
      if(this.submittedIncome.paymentReceived === true){
        this.paymentReceivedValue = this.submittedIncome.dateOfPayment;
      }else{
        this.paymentReceivedValue = 'No';
      }
    }
    }

    goToDashboard(){
      this.router.navigate(['dashboard']);
    }

    goToIncomeSummary(){
      this.router.navigate(['income-summary', this.userId])
    }

    refreshForm() {
      this.hideForm = false;
    }

  }
