import { Component, OnInit } from '@angular/core';
import { IncomeService } from '../service/income.services';
import { ActivatedRoute, Router } from '@angular/router';
import { EditIncome } from '../models/edit-income';

@Component({
  selector: 'app-income-details',
  templateUrl: './income-details.component.html',
  styleUrls: ['./income-details.component.css']
})
export class IncomeDetailsComponent implements OnInit {
  showDateOfPayment: boolean;
  userDetails: any;
  id: number;
  incomeObject: any;
  disabled: boolean = true;
  income: EditIncome;
  paymentOptions: any = [{ value: true, text: "Yes", name:"yesOption"}, { value: false, text:"No", name:"noOption"}];

  constructor(private incomeService: IncomeService, private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {

    this.userDetails = (JSON.parse(localStorage.getItem('currentUser')));
    this.activatedRoute.params.subscribe(paramsId => {
    this.id = paramsId.id;
    });
    console.log(this.id);
    let invoiceId = +this.id;

    this.incomeService.getInvoiceById(invoiceId).subscribe(
      response => {
        console.log(response); 
        if(response){
          this.incomeObject = response

          }
        }
    )
  }

  submitForm(f) {
    this.income = {
        clientName : f.value.clientName || this.incomeObject.clientName,
        dateOfInvoice : f.value.dateOfInvoice || this.incomeObject.dateOfInvoice,
        invoiceValue : f.value.invoiceValue ||  this.incomeObject.invoiceValue,
        notes : f.value.notes || this.incomeObject.notes,
        paymentReceived : this.incomeObject.paymentReceived,
        dateOfPayment: f.value.dateOfPayment || this.incomeObject.dateOfPayment,
        userId : this.userDetails.userId,
        id: this.incomeObject.id
      }

      console.log(this.income); 

      this.incomeService.updateInvoiceDetails(this.income);
      this.router.navigate(['/invoiceDetails']);
  }

  handleClick(value) {
    this.incomeObject.paymentReceived = value;
  }

  inputEnabled(){
    return this.disabled = false;
    
   }

}
