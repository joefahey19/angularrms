import { Component } from '@angular/core';
import { AuthService } from './service/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'RMS App'; 
  userDetails : any;
  userId: number;

  constructor(public auth:AuthService, private router: Router) {

  }

  ngOnInit() {
    this.userDetails = (JSON.parse(localStorage.getItem('currentUser')));

  }

  logout() {
    localStorage.removeItem('currentUser');
  }

  addIncome(){
    this.userDetails = (JSON.parse(localStorage.getItem('currentUser')));
    this.router.navigate(['income', this.userDetails.id]);
  }

  incomeSummary(){
    this.userDetails = (JSON.parse(localStorage.getItem('currentUser')));
    this.router.navigate(['income-summary', this.userDetails.id]);
  }

  addExpense(){
    this.userDetails = (JSON.parse(localStorage.getItem('currentUser')));
    this.router.navigate(['expense', this.userDetails.id]);
  }

  expenseSummary(){
    this.userDetails = (JSON.parse(localStorage.getItem('currentUser')));
    this.router.navigate(['expense-summary', this.userDetails.id]);
  }

  goToLink(url: string){
    window.open(url, "_blank");
}

}
 
