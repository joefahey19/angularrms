import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../service/user.services';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.css']
})
export class BarChartComponent implements OnInit {
  @Input() userId;
  userDetails : any;
  upToDateUser : any;
  income: any;
  barChartData: any[];
  mbarChartLabels:string[];
  barChartOptions: any;
  barChartType: any;
  barChartLegend: boolean;
  barChartColors:Array<any>;
  chartReady: boolean = false;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userDetails = (JSON.parse(localStorage.getItem('currentUser')));
    this.userService.updateUser(this.userDetails.id).subscribe(
      response => { this.upToDateUser = response;
        this.setUpBarCharts();
      }
    ) 
  }

    setUpBarCharts() {
      this.barChartOptions = {
        scaleShowVerticalLines: false,
        responsive: true, 
        scales : {
          yAxes: [{
             ticks: {
                steps : 10,
                stepValue : 500,
                max : this.upToDateUser.income + 1000,
                min: 0
              }
          }]
        }
      };
    
        this.mbarChartLabels = [ 'Income vs Expenses'];
        this.barChartType = 'bar';
        this.barChartLegend = true;
      
        this.barChartColors = [
        {
          backgroundColor: 'rgba(105,159,177,0.2)',
          borderColor: 'rgba(105,159,177,1)',
          pointBackgroundColor: 'rgba(105,159,177,1)',
          pointBorderColor: '#fafafa',
          pointHoverBackgroundColor: '#fafafa',
          pointHoverBorderColor: 'rgba(105,159,177)'
        },
        { 
          backgroundColor: 'rgba(77,20,96,0.3)',
          borderColor: 'rgba(77,20,96,1)',
          pointBackgroundColor: 'rgba(77,20,96,1)',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgba(77,20,96,1)'
        }
      ];

    this.barChartData = [
      {data: [this.upToDateUser.income], label: 'Income'}, 
      {data: [this.upToDateUser.outgoing], label: 'Expense'}
      ];

      this.chartReady = true;
    }

    // events
    public chartClicked(e:any):void {
      console.log(e);
    }
  
    public chartHovered(e:any):void {
      console.log(e);
    }

  

}
