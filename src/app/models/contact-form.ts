export class ContactForm{
    name: string;
    email: string;
    phone: number;
    message: string;
}