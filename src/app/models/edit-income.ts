export class EditIncome{

    clientName: string;
    dateOfInvoice : Date;
    invoiceValue : number;
    paymentReceived : boolean;
    dateOfPayment : string;
    notes : string;
    userId : number;
    id: number;
    
    }