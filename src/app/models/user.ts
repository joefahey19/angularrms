
export class User{

    firstName : String;
    lastName : String;
    jobTitle : String;
    password : String;
    emailAddress : String;
    isActive : boolean;
    isAdmin : boolean;
    income : number;
    outgoing : number;
    dateOfBirth: String;
    taxNo: String;

}
	