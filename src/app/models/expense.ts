export class Expense{
    expenseType: string;
    dateOfExpense: string;
    expenseValue: number;
    notes: string;
    userId: number;
}