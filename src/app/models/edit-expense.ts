export class ExpenseDetails{
    expenseType: string;
    dateOfExpense: string;
    expenseValue: number;
    notes: string;
    userId: number;
    id: number;
}