export class Income{

clientName: string;
dateOfInvoice : Date;
invoiceValue : number;
paymentReceived : boolean;
dateOfPayment : string;
notes : string;
userId : number;

}