import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Auth } from '../models/auth';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getUsers() {
    return this.http.get('http://localhost:8080/findall');
  }

  postUser(user){
    return this.http.post('http://localhost:8080/createUser', user);
  }

  updateDetails(user){
    return this.http.post('http://localhost:8080/updateUserDetails', user).subscribe(
      (response) => console.log(response),
      (error) => console.log(error))
  }

  updateUser(userId){
    return this.http.post('http://localhost:8080/findById', userId);
  }

  authenticateUser(auth){
    return this.http.post('http://localhost:8080/authenticate', auth);
  }
}
