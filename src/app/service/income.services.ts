import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class IncomeService {

  incomeId: any;
  constructor(private http: HttpClient) { }


  postIncome(income){
    return this.http.post('http://localhost:8080/createIncome', income);   
  }

  getIncomeId() {
    return this.incomeId;
  }

  getByIncomeId(id) {
    return this.http.post('http://localhost:8080/incomeSuccess', id);
  }

  getIncomeByUserId(userId){
    return this.http.post('http://localhost:8080/incomeSummary', userId);
  }

  getInvoiceById(id){
    return this.http.post('http://localhost:8080/getInvoiceById', id);

  }

  updateInvoiceDetails(income){
    return this.http.post('http://localhost:8080/updateInvoiceDetails', income).subscribe(
      (response) => console.log(response),
      (error) => console.log(error))
  }

  deleteIncome(id) {
    return this.http.post('http://localhost:8080/deleteIncome', id);
  }
}
