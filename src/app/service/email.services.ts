import { RegisterEmail } from './../models/register-email';
import { ContactForm } from './../models/contact-form';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmailService {

  constructor(private http: HttpClient) { }

  postContactForm(contactForm){
    return this.http.post('http://localhost:8080/contactEmail', contactForm).subscribe(
        (response) => console.log(response),
        (error) => console.log(error))
  }

  postRegisterEmail(registerEmail){
    return this.http.post('http://localhost:8080/register',registerEmail).subscribe(
        (response) => console.log(response),
        (error) => console.log(error))

  }

}
