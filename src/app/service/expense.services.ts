import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ExpenseService {

  constructor(private http: HttpClient) { }


  postExpense(expense){
    
    return this.http.post('http://localhost:8080/createExpense', expense);
  }

  getExpenseByUserId(userId){
    return this.http.post('http://localhost:8080/expenseSummary', userId);
  }

  getExpenseById(id){
    return this.http.post('http://localhost:8080/getExpenseById', id);
  }

  updateExpenseDetails(expense){
    return this.http.post('http://localhost:8080/updateExpenseDetails', expense).subscribe(
      (response) => console.log(response),
      (error) => console.log(error))
  }

  deleteExpense(id) {
    return this.http.post('http://localhost:8080/deleteExpense', id);
  }

}

