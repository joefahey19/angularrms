
import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {
  constructor() {}

  public isAuthenticated(): boolean {
    const user = (JSON.parse(localStorage.getItem('currentUser')));
    if (user) {
        return true;
    } else {
    return false;
    }
  }
}