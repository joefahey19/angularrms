import { UserService } from './../service/user.services';
import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {
    @Input() userId;
    userDetails : any;
    upToDateUser : any;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    
    this.userDetails = (JSON.parse(localStorage.getItem('currentUser')));
    this.userService.updateUser(this.userDetails.id).subscribe(
        response => this.upToDateUser = response
    )}

    yourDetails(){
      this.router.navigate(['userDetails']);
    }

    addIncome(userId){
      this.userId = userId;
      this.router.navigate(['income', this.userId]);
    }

    addExpense(userId){
      this.userId = userId;
      this.router.navigate(['expense', this.userId]);
    }

}


