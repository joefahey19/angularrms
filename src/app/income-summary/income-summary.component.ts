import { Component, OnInit,} from '@angular/core';
import { IncomeService } from '../service/income.services';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmationDialogService } from '../confirmation-dialog/confirmation-dialog.component.service';


@Component({
  selector: 'app-income-summary',
  templateUrl: './income-summary.component.html',
  styleUrls: ['./income-summary.component.css']
})

export class IncomeSummaryComponent implements OnInit {
    incomeList: any;
    id: number;
    userId: number;

  constructor(private incomeService: IncomeService, private activatedRoute: ActivatedRoute, private router: Router, private confirmationDialogService: ConfirmationDialogService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(paramsId => {
      this.userId = +paramsId.userId;
      });

    this.getIncomeSummary();

  }

  getIncomeSummary() {
    this.incomeService.getIncomeByUserId(this.userId).subscribe(
      response => {
          console.log(response);
          if(response){
              this.incomeList = response
          }
      });
  }
  
  invoiceDetails(invoiceId){
    this.id = invoiceId;
    this.router.navigate(['invoiceDetails', this.id]);
  }

  
  public openConfirmationDialog(id) {
    this.confirmationDialogService.confirm('Please confirm..', 'Do you really want to delete this invoice? This cannot be undone.')
    .then((confirmed) => this.deleteIncome(id))
    .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
  }

  deleteIncome(id) {
    this.incomeService.deleteIncome(id).subscribe(
      response => {
        console.log(response);
        this.getIncomeSummary();
        }
    )
  }

  addIncome(userId){
    this.userId = userId;
    this.router.navigate(['income', this.userId]);
  }

  goToDashboard(){
    this.router.navigate(['dashboard']);
  }

  }
