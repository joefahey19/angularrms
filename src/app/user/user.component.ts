import { Component, OnInit } from '@angular/core';
import { UserService } from '../service/user.services';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  users: any;
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.getUsers().subscribe(
     response =>this.handleSuccessfulResponse(response),
    );
  }

  
handleSuccessfulResponse(response)
{
    this.users=response;
    console.log(this.users);
}



}
